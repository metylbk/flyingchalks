<?php

namespace Pingpong\Admin\Repositories\Promotions;

use Pingpong\Admin\Repositories\Repository;

interface PromotionRepository extends Repository
{
    public function getArticle();
}
