<?php

namespace Pingpong\Admin\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Pingpong\Admin\Uploader\ImageUploader;
use Pingpong\Admin\Validation\Promotion\Create;
use Pingpong\Admin\Validation\Promotion\Update;
use Pingpong\Admin\Repositories\PromotionRepository;


class PromotionsController extends BaseController
{
    protected $promotions;

    /**
     * @var ImageUploader
     */
    protected $uploader;

    /**
     * @param ImageUploader $uploader
     */
    public function __construct(ImageUploader $uploader)
    {
        $this->uploader = $uploader;

        $this->repository = $this->getRepository();
    }

    /**
     * Get repository instance.
     *
     * @return mixed
     */
    public function getRepository()
    {
        $repository = 'Pingpong\Admin\Repositories\Promotions\PromotionRepository';

        return app($repository);
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return $this->redirect('promotions.index')
            ->withFlashMessage('Promotion not found!')
            ->withFlashType('danger');
    }

    /**
     * Display a listing of promotions
     *
     * @return Response
     */
    public function index()
    {
        $promotions = $this->repository->allOrSearch(Input::get('search'));

        $no = $promotions->firstItem();

        return $this->view('promotions.index', compact('promotions', 'no'));
    }

	/**
     * Show the form for creating a new promotion
     *
     * @return Response
     */
    public function create()
    {
        return $this->view('promotions.create');
    }

    /**
     * Store a newly created promotion in storage.
     *
     * @return Response
     */
    public function store(Create $request)
    {
		$data = $request->all();

        unset($data['company_logo']);

        if (\Input::hasFile('company_logo')) {
            // upload company logo
            $this->uploader->upload('company_logo')->save('images/promotions');

            $data['company_logo'] = $this->uploader->getFilename();
        }

        $this->repository->create($data);

        return $this->redirect('promotions.index');
    }

    /**
     * Display the specified promotion.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $promotion = $this->repository->findById($id);

            return $this->view('promotions.show', compact('promotion'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Show the form for editing the specified promotion.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $promotion = $this->repository->findById($id);

            return $this->view('promotions.edit', compact('promotion'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified promotion in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        try {
            $promotion = $this->repository->findById($id);

            $data = $request->all();

            unset($data['company_logo']);

            if (\Input::hasFile('company_logo')) {
                // $promotion->deleteCompanyLogo();
                $file = 'images/promotions/'.$promotion->company_logo;
                if (\Storage::disk('uploads')->exists($file)) {
                    \Storage::disk('uploads')->delete($file);
                }

                $this->uploader->upload('company_logo')->save('images/promotions');

                $data['company_logo'] = $this->uploader->getFilename();
            }

            $promotion->update($data);

            return $this->redirect('promotions.index');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Remove the specified promotion from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $promotion = $this->repository->findById($id);
            // $promotion->deleteCompanyLogo();
            $file =  'images/promotions/'.$promotion->company_logo;
            if (\Storage::disk('uploads')->exists($file)) {
                \Storage::disk('uploads')->delete($file);
            }

            $this->repository->delete($id);

            return $this->redirect('promotions.index');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }
}
