<?php

namespace Pingpong\Admin\Composers;

use App\PromotionType;

class PromotionFormComposer
{
    public function compose($view)
    {
        $types = PromotionType::lists('name', 'id');

        $view->with(compact('types'));
    }
}
