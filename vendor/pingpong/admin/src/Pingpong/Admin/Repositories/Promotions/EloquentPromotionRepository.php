<?php

namespace Pingpong\Admin\Repositories\Promotions;

use Pingpong\Admin\Entities\Promotion;

class EloquentPromotionRepository implements PromotionRepository
{
    public function perPage()
    {
        return config('admin.promotion.perpage');
    }

    public function getModel()
    {
        $model = config('admin.promotion.model');

        return new $model;
    }

    public function getArticle()
    {
        return $this->getModel();
    }

    public function allOrSearch($searchQuery = null)
    {
        if (is_null($searchQuery)) {
            return $this->getAll();
        }

        return $this->search($searchQuery);
    }

    public function getAll()
    {
        return $this->getArticle()->latest()->paginate($this->perPage());
    }

    public function search($searchQuery)
    {
        $search = "%{$searchQuery}%";

        return $this->getArticle()->where('code', 'like', $search)
            ->orWhere('company_name', 'like', $search)
            ->orWhere('company_description', 'like', $search)
            ->orWhere('offer_name', 'like', $search)
            ->orWhere('offer_description', 'like', $search)
            ->paginate($this->perpage());
    }

    public function findById($id)
    {
        return $this->getArticle()->find($id);
    }

    public function findBy($key, $value, $operator = '=')
    {
        return $this->getArticle()->where($key, $operator, $value)->paginate($this->perPage());
    }

    public function delete($id)
    {
        $article = $this->findById($id);

        if (!is_null($article)) {
            $article->delete();

            return true;
        }

        return false;
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
}
