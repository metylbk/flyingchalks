<?php namespace Pingpong\Admin\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Pingpong\Admin\Repositories\Users\UserRepository;
use Pingpong\Admin\Validation\User\Create;
use Pingpong\Admin\Validation\User\Update;
use Illuminate\Http\Request;

use DB;
use App\ExchangeStudent;
use App\University;
use App\City;
use App\Country;
use App\UserType;
use Datetime;
class UsersController extends BaseController
{

    /**
     * @var \User
     */
	 private $exchangeStudent;
    protected $users;

    /**
     * @param \User $users
     */
    public function __construct(UserRepository $repository,ExchangeStudent $exchangeStudent)
    {
        $this->repository = $repository;
		$this->exchangeStudent = $exchangeStudent;
    }

    /**
     * Redirect not found.
     *
     * @return Response
     */
    protected function redirectNotFound()
    {
        return $this->redirect('users.index');
    }

    /**
     * Display a listing of users
     *
     * @return Response
     */
    public function index(Request $request)
    {

        /*$users = $this->repository->allOrSearch(Input::get('q'));
		
        $no = $users->firstItem();*/
		$exchangeDetail = ExchangeStudent::lists('exchangeTerm','exchangeTerm')->toArray();
		
		$exchangeDetail=array_filter($exchangeDetail);
		
		
      	
		$homeUnivList = University::whereIn('cityID', array('147'))->orderBy('universityName')->lists('universityName','id')->toArray();
	
		//$homeUnivList = University::whereIn('universityName', $this->listOfSgUniversity())->orderBy('universityName')->lists('universityName', 'id');
		
        $homeUnivList = $this->changeOrderForUniversityOthersOption($homeUnivList, false);
		
		$hostUnivList = University::whereNotIn('cityID',  array('147'))->orderBy('universityName')->lists('universityName', 'id')->toArray();
		
		$hostUnivList = $homeUnivList + $hostUnivList;
		
		
		$homeUnivList = $hostUnivList;
		
		
        $countryList = Country::orderBy('countryName')->lists('countryName','countryID')->toArray();

        $usertypeList = UserType::orderBy('id')->lists('title','id')->toArray();

       	
		
		$data = $request->all();
		
		$conditionsArr = array();
	
		if(@count($data)>0 && isset($data['search'])){

			$search = $data['search'];	
			$usertype = $data['usertype'];
			$hostUniv = $data['hostUniv'];
			$homeUniv = $data['homeUniv'];
			$hostCountry = $data['hostCountry'];
			$exchangeterm = $data['exchangeterm'];
			$last_logged_in = $data['last-logged-in'];

			if($usertype=='profile_Not_updated'){
					
				if($last_logged_in){

					$start_date = new \DateTime();
					$now_date = new \Datetime();
					$now_d = $now_date->format('Y-m-d H:i:s');
					$dt = $start_date->createFromFormat('m/d/Y', $last_logged_in);
					$serdate = $dt->format('Y-m-d 00:00:00');

					$users = DB::table('users')
						->select(array('users.*', DB::raw('0 AS sentSaTotal'), DB::raw('0 AS sentBfTotal'), DB::raw('0 AS receiveBfTotal'), DB::raw('0 AS receiveSaTotal')))
						->whereNotIn('id',function($q){
	   						$q->select('user_id')->from('exchangestudents');
						})
						->whereBetween('users.last_logged_in', array($serdate, $now_d))
						->orderBy('users.last_logged_in', 'DESC')
						->orderBy('users.fname','ASC')
						->paginate(config('admin.user.perpage'));
				}else{
					$users = DB::table('users')
						->select(array('users.*', DB::raw('0 AS sentSaTotal'), DB::raw('0 AS sentBfTotal'), DB::raw('0 AS receiveBfTotal'), DB::raw('0 AS receiveSaTotal')))
						->whereNotIn('id',function($q){
	   						$q->select('user_id')->from('exchangestudents');
						})
						->orderBy('users.last_logged_in', 'DESC')
						->orderBy('users.fname','ASC')
						->paginate(config('admin.user.perpage'));
				}
			}else{

				
				$query = "SELECT users.id  FROM users INNER JOIN role_user ON role_user.user_id = users.id    ";

				$joinQuery ='';
				$filter = ' where role_user.role_id= 2  ';

				if($usertype || $hostUniv || $homeUniv || $hostCountry || $exchangeterm){
					$joinQuery .=' INNER JOIN exchangestudents ON exchangestudents.user_id = users.id '; 
				}

				if($usertype){
					$joinQuery .= ' INNER JOIN user_types ON exchangestudents.type = user_types.id  ';
					$filter .=  " and user_types.id =".$usertype; 
				}

				if($hostUniv){
					$joinQuery .= ' INNER JOIN universities AS host ON exchangestudents.hostUniversityID = host.id  ';
					$filter .= " and host.id =".$hostUniv;
				}

				if($homeUniv){
					$joinQuery .= ' INNER JOIN universities AS home ON exchangestudents.homeUniversityID = home.id ';
					$filter .= " and home.id =".$homeUniv;
				}		

				if($hostCountry){
					if(!$hostUniv){
						$joinQuery .= ' INNER JOIN universities AS host ON exchangestudents.hostUniversityID = host.id  ';
					}
					$joinQuery .= ' INNER JOIN cities ON host.cityID = cities.id INNER JOIN countries ON cities.countryID = countries.countryID ';
					$filter .= " and countries.countryID = '".$hostCountry."' ";
				}

				if($exchangeterm){
					$filter .= " and exchangestudents.exchangeTerm = '".$exchangeterm."'  ";
				}


				if($search){
					$search = "%$search%";
					$filter .= " and (users.fname like '".$search."'  or users.lname like '".$search."' or users.email like '".$search."') ";
				}



				if($last_logged_in){
					$start_date = new \DateTime();
					$now_date = new \Datetime();
					$now_d = $now_date->format('Y-m-d H:i:s');

					$dt = $start_date->createFromFormat('m/d/Y', $last_logged_in);
					$serdate = $dt->format('Y-m-d 00:00:00');
					$filter .= " and date(users.last_logged_in) between  date('".$serdate."') and date('".$now_d."')  ";

					//$filter .= " and date(users.last_logged_in) ='".$serdate."' ";
				}



				$query = $query.$joinQuery.$filter;

			
				
				$usersListdata = DB::select($query);
			
				

				$usersList =array();		
				foreach ($usersListdata as $value) {
		    		$usersList[] = $value->id;
				}
					
		
			
			  	$users = DB::table('role_user')
						->select(array('users.*', DB::raw('IFNULL(SCC.sSaTotal, 0) AS sentSaTotal'), DB::raw('IFNULL(SC.sBfTotal, 0) AS sentBfTotal'), DB::raw('IFNULL(RC.rBfTotal, 0) AS receiveBfTotal'), DB::raw('IFNULL(RCC.rSaTotal, 0) AS receiveSaTotal')))
						->join('users', 'role_user.user_id', '=', 'users.id')	
						->leftJoin(DB::raw('(SELECT type,user_id, COUNT(*) as sBfTotal FROM user_requests where type = "1" GROUP BY user_id, type) AS SC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'SC.user_id' );
							})
						->leftJoin(DB::raw('(SELECT type,user_id, COUNT(*) as sSaTotal FROM user_requests where type = "2" GROUP BY user_id, type) AS SCC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'SCC.user_id' );
							})	
						->leftJoin(DB::raw('(SELECT type,to_id, COUNT(*) as rBfTotal FROM user_requests where type = "1" GROUP BY to_id,  type) AS RC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'RC.to_id' );
							})	
						->leftJoin(DB::raw('(SELECT type,to_id, COUNT(*) as rSaTotal FROM user_requests where type = "2" GROUP BY to_id,  type) AS RCC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'RCC.to_id' );
							})		
						->where('role_id','2')
						->whereIn('role_user.user_id',$usersList)
						->orderBy('users.last_logged_in', 'DESC')
						->orderBy('users.fname','ASC')
						->paginate(config('admin.user.perpage'));
			}

		}else{
				$users = DB::table('role_user')
						->select(array('users.*', DB::raw('IFNULL(SCC.sSaTotal, 0) AS sentSaTotal'), DB::raw('IFNULL(SC.sBfTotal, 0) AS sentBfTotal'), DB::raw('IFNULL(RC.rBfTotal, 0) AS receiveBfTotal'), DB::raw('IFNULL(RCC.rSaTotal, 0) AS receiveSaTotal')))
						->join('users', 'role_user.user_id', '=', 'users.id')	
						->leftJoin(DB::raw('(SELECT type,user_id, COUNT(*) as sBfTotal FROM user_requests where type = "1" GROUP BY user_id, type) AS SC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'SC.user_id' );
							})
						->leftJoin(DB::raw('(SELECT type,user_id, COUNT(*) as sSaTotal FROM user_requests where type = "2" GROUP BY user_id, type) AS SCC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'SCC.user_id' );
							})	
						->leftJoin(DB::raw('(SELECT type,to_id, COUNT(*) as rBfTotal FROM user_requests where type = "1" GROUP BY to_id,  type) AS RC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'RC.to_id' );
							})	
						->leftJoin(DB::raw('(SELECT type,to_id, COUNT(*) as rSaTotal FROM user_requests where type = "2" GROUP BY to_id,  type) AS RCC'), function( $query ){
								$query->on( 'role_user.user_id', '=', 'RCC.to_id' );
							})		
						->where('role_id','2')
						->paginate(config('admin.user.perpage'));
						
						
			//echo '<pre>';print_r($users);die;
	
		}
		
		
		
		$no = $users->firstItem();
		
		//echo '<pre>';print_r($users);die;
        return $this->view('users.index', compact('users', 'no','hostUnivList','homeUnivList','countryList','usertypeList','exchangeDetail'));
    }



    /**
     * Show the form for creating a new user
     *
     * @return Response
     */
    public function create()
    {
    
        return $this->view('users.create');
    }

    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
    public function store(Create $request)
    {
        $data = $request->all();
		
        $user = $this->repository->create($data);

        $user->addRole($request->get('role'));

        return $this->redirect('users.index');
    }

    /**
     * Display the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $user = $this->repository->findById($id);
            return $this->view('users.show', compact('user'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }



  
	

    /**
     * Show the form for editing the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $user = $this->repository->findById($id);
			
			$exchangeDetail = ExchangeStudent::whereUserId($id)->with(['homeUniversity','hostUniversity','hostUniversity.city.country'])->first();
		
			$homeUnivList = University::whereIn('cityID', array('147'))->orderBy('universityName')->lists('universityName','id')->toArray();

			$homeUnivList = $this->changeOrderForUniversityOthersOption($homeUnivList, false);
		
			$hostUnivList = University::whereNotIn('cityID',  array('147'))->orderBy('universityName')->lists('universityName', 'id')->toArray();
			
			$hostUnivList = $homeUnivList + $hostUnivList;
		
		
			$homeUnivList = $hostUnivList;
		
		
		//$hostUnivList = $this->changeOrderForUniversityOthersOption($hostUnivList, true);

			$countryList = Country::orderBy('countryName')->lists('countryName', 'countryID')->toArray();
			
			$userTypes = UserType::orderBy('id')->lists('title', 'id')->toArray();
			
            $role = $user->roles->lists('id');
			
            return $this->view('users.edit', compact('user', 'role','homeUnivList', 'hostUnivList', 'countryList','exchangeDetail','userTypes'));
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Update the specified user in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        try {
				
			
            $data = ! $request->has('password') ? $request->except('password') : $this->inputAll();
            
            $user = $this->repository->findById($id);
           
            $user->update($data);

            $user->roles()->sync((array) \Input::get('role'));
			
			//insert a row in exchangestuent if admin update user profile
			$user_id = $id;
            $student = ExchangeStudent::whereUserId($user_id)->first();
			$studentArr = array();
			
			$studentType = $request->type;
		
			$studentArr['user_id'] = $user_id;
			$studentArr['homeUniversityID'] = $request->homeUniversityID;
			$studentArr['exchangeTerm'] = $request->exchangeTerm;
			$studentArr['matriculationYear'] = $request->matriculationYear;
			$studentArr['hostUniversityID'] = $request->hostUniversityID;
			$studentArr['type'] = (int)$studentType;
			
			
			// user choose 'Others" home university option
			if ($studentArr['homeUniversityID'] == 1) {
				// create new host city
				$cityName = $this->nameize($request->homecity);
				$isCityExist = City::where('cityName', 'like', '%' . $cityName . '%')->count();
				// check if city exist
				if ($isCityExist == 0) {
					$newCity = ['cityName' => $cityName, 'countryID' => $request->homecountry];
					City::create($newCity);
				}

				$universityName = $this->nameize($request->homeuniversityName);
				// check if university exist
				$isUniversityExist = University::where('universityName', 'like', '%' . $universityName . '%')->count();
				if ($isUniversityExist == 0) {
					// get the city ID
					$cityID = City::select("id")->where("cityName", $cityName)->first();
					$newUniversity = ['universityName' => $universityName, 'cityID' => $cityID->id];
					University::create($newUniversity);
					$newUniID = University::select("id")->where('universityName', '=', $universityName)->first();
					$studentArr['homeUniversityID'] = $newUniID->id;
				}
			}
			
			
			if(in_array($studentType,array('1','2'))){
				// user choose 'Others" host university option
				if ($studentArr['hostUniversityID'] == 1) {
					// create new host city
					$cityName = $this->nameize($request->hostcity);
					$isCityExist = City::where('cityName', 'like', '%' . $cityName . '%')->count();
					// check if city exist
					if ($isCityExist == 0) {
						$newCity = ['cityName' => $cityName, 'countryID' => $request->hostCountry];
						City::create($newCity);
					}

					$universityName = $this->nameize($request->hostuniversityName);
					// check if university exist
					$isUniversityExist = University::where('universityName', 'like', '%' . $universityName . '%')->count();
					if ($isUniversityExist == 0) {
						// get the city ID
						$cityID = City::select("id")->where("cityName", $cityName)->first();
						$newUniversity = ['universityName' => $universityName, 'cityID' => $cityID->id];
						University::create($newUniversity);
						$newUniID = University::select("id")->where('universityName', '=', $universityName)->first();
						$studentArr['hostUniversityID'] = $newUniID->id;
						
					}
				}
			}
			
			
			
			if(!$student) {
				if(ExchangeStudent::create($studentArr)) {
					//Session::put('custom_success','You have successfully updated your Exchange / International studies details!');
					//return Redirect::to('/home');
				}else {
					//Session::put('custom_error','Some problem occur. Please try again!');	
				}
			}else {
				if(ExchangeStudent::where('user_id', $studentArr['user_id'])->update($studentArr)) {
					//Session::put('custom_success','You have successfully updated your Exchange / International studies details!');
					//return Redirect::to('/home');
				}else {
					//Session::put('custom_error','Some problem occur. Please try again!');
				}
			}
			
			return redirect()->away($request->input('redirects_to'));
            //return $this->redirect('users.index');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
			
            $this->repository->delete($id);

            return $this->redirect('users.index');
        } catch (ModelNotFoundException $e) {
            return $this->redirectNotFound();
        }
    }
		public function changeOrderForUniversityOthersOption($listOfUni, $isTop)
    {
        $others = array_search('Others', $listOfUni);
        // remove others from the array
        unset($listOfUni[$others]);
        if ($isTop) {
            // put others option at the top of the array
            $first = array();
            $first[$others] = "Others";
            $listOfUni = $first + $listOfUni;
        } else {
            // put others at the end of the array
            $listOfUni[$others] = "Others";
        }
        return $listOfUni;
    }


	
}
