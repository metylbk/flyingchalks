<?php

namespace Pingpong\Admin\Presenters;

use Pingpong\Presenters\Presenter;

class Promotion extends Presenter
{
    public function companyLogoPath()
    {
        return public_path("images/promotions/{$this->entity->company_logo}");
    }
}
