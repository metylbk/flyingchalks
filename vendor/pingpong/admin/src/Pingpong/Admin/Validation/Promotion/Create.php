<?php

namespace Pingpong\Admin\Validation\Promotion;

use Pingpong\Admin\Validation\Validator;

class Create extends Validator
{
    protected $rules = [
        'code' => 'required',
        'company_name' => 'required',
        'company_logo' => 'required|image',
        'offer_name' => 'required',
        'discount' => 'required',
        'type_id' => 'required|integer',
    ];

    public function rules()
    {
        return $this->rules;
    }
}
