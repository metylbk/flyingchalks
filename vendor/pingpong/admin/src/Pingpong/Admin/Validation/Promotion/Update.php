<?php

namespace Pingpong\Admin\Validation\Promotion;

use Pingpong\Admin\Validation\Validator;
use Illuminate\Support\Facades\Request;

class Update extends Validator
{
    public function rules()
    {
        return [
            'code' => 'required',
            'company_name' => 'required',
            'offer_name' => 'required',
            'discount' => 'required',
            'type_id' => 'required|integer',
        ];
    }
}
