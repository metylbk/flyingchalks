<?php

namespace Pingpong\Admin\Entities;

use Pingpong\Presenters\Model;

class Promotion extends Model
{
    /**
     * @var string
     */
    protected $presenter = 'Pingpong\Admin\Presenters\Promotion';

    protected $fillable = [
        'code',
        'company_name',
        'company_description',
        'company_logo',
        'offer_name',
        'offer_description',
        'discount',
        'type_id',
    ];

    public function type()
    {
        return $this->belongsTo('App\PromotionType');
    }

    /**
     * @return bool
     */
    public function deleteCompanyLogo()
    {
        $file = $this->present()->companyLogoPath;

        if (file_exists($file)) {
            @unlink($file);

            return true;
        }

        return false;
    }
}
